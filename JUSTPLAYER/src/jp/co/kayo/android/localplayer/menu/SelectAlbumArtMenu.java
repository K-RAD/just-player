package jp.co.kayo.android.localplayer.menu;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.ArrayList;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.SystemConsts;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;

import android.view.MenuItem;
import android.view.SubMenu;
import android.view.MenuItem.OnMenuItemClickListener;

public class SelectAlbumArtMenu extends BaseActionProvider implements OnMenuItemClickListener {
    Handler handler;

    public SelectAlbumArtMenu(Context context) {
        super(context);
    }

    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    @Override
    public void onPrepareSubMenu(SubMenu subMenu) {
        subMenu.clear();
        
        subMenu.addSubMenu(R.id.mnu_select_albumart, R.id.mnu_select_albumart+0, 0, context.getString(R.string.sub_mnu_albumart_search));
        subMenu.addSubMenu(R.id.mnu_select_albumart, R.id.mnu_select_albumart+1, 1, context.getString(R.string.sub_mnu_albumart_local));
        
        for (int i = 0; i < subMenu.size(); ++i) {
            subMenu.getItem(i).setOnMenuItemClickListener(this);
        }
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        int itemid = item.getItemId();
        doItemSelect(itemid, item.getTitle().toString());
        return true;
    }

    @Override
    public boolean onPerformDefaultAction() {
        if (Build.VERSION.SDK_INT < 11) {
            ArrayList<String> menuItems = new ArrayList<String>();
            menuItems.add(context.getString(R.string.sub_mnu_albumart_search));
            menuItems.add(context.getString(R.string.sub_mnu_albumart_local));

            final CharSequence[] items = menuItems.toArray(new CharSequence[menuItems.size()]);
            new AlertDialog.Builder(context)
                    .setTitle(context.getString(R.string.mnu_select_albumart))
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            doItemSelect(R.id.mnu_select_albumart+item, items[item].toString());
                        }
                    })
                    .show();
        }
        return super.onPerformDefaultAction();
    }

    
    private void doItemSelect(int itemid, String item_name){
        if (item_name.startsWith(context.getString(R.string.sub_mnu_albumart_search))) {
            handler.sendMessage(handler.obtainMessage(SystemConsts.EVT_SELECT_ALBUMART_WEB, Integer.valueOf(1)));
        }
        else if (item_name.startsWith(context.getString(R.string.sub_mnu_albumart_local))) {
            handler.sendMessage(handler.obtainMessage(SystemConsts.EVT_SELECT_ALBUMART_LOCAL, Integer.valueOf(2)));
        }
    }
}
