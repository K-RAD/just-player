package jp.co.kayo.android.localplayer.dialog;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.util.Logger;
import jp.co.kayo.android.localplayer.util.LyricsHelper;
import jp.co.kayo.android.localplayer.util.ThemeHelper;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.PluginState;

public class LyricsDialog extends DialogFragment {
    String mTitle;
    String mArtist;
    WebView mWebView;
    AsyncTask<Void, Void, Void> mTask;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.plugin_wikipedia, null, false);

        // 縦方向の時はこれらのViewは全てnullになっていることに注意。
        mWebView = (WebView) view.findViewById(R.id.webView1);

        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setPluginState(PluginState.ON);
        mWebView.getSettings().setSupportZoom(true);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (url.startsWith("http://lyrics.wikia.com/")) {
                    // 日本語歌詞を自動的に開く
                    String showFirstTab = "javascript:(function(){var b=document.querySelector('h2>.showbutton');if(b){var evt=document.createEvent('MouseEvents');evt.initEvent('click',false,true);b.dispatchEvent(evt)}})();";
                    view.loadUrl(showFirstTab);
                }
            }
        });
        mWebView.requestFocus();

        if (getArguments() != null) {
            mTitle = getArguments().getString("title");
            mArtist = getArguments().getString("artist");
        }

        loadUrl();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setNegativeButton(getString(R.string.alert_dialog_close), null);
        builder.setView(view);
        builder.setInverseBackgroundForced(new ThemeHelper().isInverseBackgroundForced(getActivity()));

        return builder.create();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTask != null) {
            mTask.cancel(true);
        }
        if (mWebView != null) {
            this.mWebView.stopLoading();
            this.mWebView.setWebChromeClient(null);
            this.mWebView.setWebViewClient(null);
            this.unregisterForContextMenu(this.mWebView);
            this.mWebView.destroy();
            this.mWebView = null;
        }
    }

    private void loadUrl() {
        mTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                LyricsHelper helper = new LyricsHelper();
                try {
                    final String url = helper.getSong(mArtist, mTitle);
                    if (url != null) {
                        if (mWebView != null) {
                            mWebView.loadUrl(url);
                        }
                    }
                } catch (Exception e) {
                    Logger.e("loadLyrics", e);
                }
                return null;
            }

        };
        mTask.execute();

    }

}
