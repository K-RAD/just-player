package jp.co.kayo.android.localplayer.appwidget;

/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.HashMap;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.widget.TextView;

public class ColorSet {
    public static final String KEY_NOTIFI_PRI_COLOR = "key.notifi.pri.color";
    public static final String KEY_NOTIFI_SEC_COLOR = "key.notifi.sec.color";
    public static final String KEY_DEFAULT_PRI_COLOR = "key.default.pri.color";
    public static final String KEY_DEFAULT_SEC_COLOR = "key.default.sec.color";
    public static final String KEY_ACTIONBAR_PRI_COLOR = "key.actionbar.pri.color";
    public static final String KEY_ACTIONBAR_SEC_COLOR = "key.actionbar.sec.color";
    public static final String KEY_ACTIONBAR_TAB_COLOR = "key.actionbar.tab.color";

    private HashMap<String, Integer> mColoerSet = new HashMap<String, Integer>();
    
    public ColorSet(){
    }
    
    public void setColor(String key, TextView textView){
        int color = mColoerSet.get(key);;
        if(color != -1){
            textView.setTextColor(color);
        }
    }
    
    public int getColor(String key){
        return mColoerSet.get(key);
    }
    
    public void putColor(Context context, String key, int color){
        mColoerSet.put(key, color);
        
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = pref.edit();
        editor.putInt(key, color);
        editor.commit();
    }
    
    public void removeColor(Context context, String key){
        mColoerSet.remove(key);
        
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = pref.edit();
        editor.remove(key);
        editor.commit();
    }
    
    public void load(Context context){
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(context);
        mColoerSet.put(KEY_NOTIFI_PRI_COLOR, pref.getInt(KEY_NOTIFI_PRI_COLOR, -1));
        mColoerSet.put(KEY_NOTIFI_SEC_COLOR, pref.getInt(KEY_NOTIFI_SEC_COLOR, -1));
        mColoerSet.put(KEY_DEFAULT_PRI_COLOR, pref.getInt(KEY_DEFAULT_PRI_COLOR, -1));
        mColoerSet.put(KEY_DEFAULT_SEC_COLOR, pref.getInt(KEY_DEFAULT_SEC_COLOR, -1));
        mColoerSet.put(KEY_ACTIONBAR_PRI_COLOR, pref.getInt(KEY_ACTIONBAR_PRI_COLOR, -1));
        mColoerSet.put(KEY_ACTIONBAR_SEC_COLOR, pref.getInt(KEY_ACTIONBAR_SEC_COLOR, -1));
        mColoerSet.put(KEY_ACTIONBAR_TAB_COLOR, pref.getInt(KEY_ACTIONBAR_TAB_COLOR, -1));
    }
}
