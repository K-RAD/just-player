package jp.co.kayo.android.localplayer.util.bean;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.consts.MediaConsts;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

public class PlaylistInfoLoader extends AsyncTaskLoader<List<PlaylistInfo>> {
    public static final long PLAYLIST_ORDER_ID = -1000;
    public static final long PLAYLIST_ADDNEW_ID = PLAYLIST_ORDER_ID-1;
    public static final long PLAYLIST_5START_ID = PLAYLIST_ORDER_ID-2;
    public static final long PLAYLIST_4START_ID = PLAYLIST_ORDER_ID-3;
    public static final long PLAYLIST_3START_ID = PLAYLIST_ORDER_ID-4;
    public static final long PLAYLIST_2START_ID = PLAYLIST_ORDER_ID-5;
    public static final long PLAYLIST_1START_ID = PLAYLIST_ORDER_ID-6;

    public enum CallType {
        TYPE_ADDDIALOG,
        TYPE_PLAYLIST,
        TYPE_PLAYBACK,
        TYPE_FAVOLITE
    }
    
    public CallType callType = CallType.TYPE_ADDDIALOG;
    

    public PlaylistInfoLoader(Context context, CallType callType) {
        super(context);
        this.callType = callType;
    }

    @Override
    public List<PlaylistInfo> loadInBackground() {
        ArrayList<PlaylistInfo> list = new ArrayList<PlaylistInfo>();
        
        long now = Calendar.getInstance().getTimeInMillis();
        //プレイキューへの追加
        if(callType != CallType.TYPE_PLAYBACK && callType != CallType.TYPE_PLAYLIST){
            PlaylistInfo item = new PlaylistInfo();
            item.id = PLAYLIST_ORDER_ID;
            item.name = getContext().getString(R.string.lb_add_playback);
            item.modified = 0;
            list.add(item);
        }
        
        //新規
        PlaylistInfo item = new PlaylistInfo();
        item.id = PLAYLIST_ADDNEW_ID;
        if(callType != CallType.TYPE_PLAYLIST){
            item.name = getContext().getString(R.string.lb_addnewplaylist);
        }else{
            item.name = getContext().getString(R.string.lb_newnewplaylist);
        }
        item.modified = 0;
        list.add(item);
        
        //5StarRated
        if(callType == CallType.TYPE_PLAYLIST){
            //PlaylistView
            PlaylistInfo item1 = new PlaylistInfo();
            item1.id = PLAYLIST_5START_ID;
            item1.name = getContext().getString(R.string.txt_playlist_name_5star);
            item1.modified = now;
            list.add(item1);
            
            PlaylistInfo item2 = new PlaylistInfo();
            item2.id = PLAYLIST_3START_ID;
            item2.name = getContext().getString(R.string.txt_playlist_name_3star);
            item2.modified = now;
            list.add(item2);
        }else {
            //PlaylistView
            PlaylistInfo item1 = new PlaylistInfo();
            item1.id = PLAYLIST_5START_ID;
            item1.name = getContext().getString(R.string.txt_playlist_name_5star);
            item1.modified = now;
            list.add(item1);
        }
        //
        //Receentry Added
        //Recentry Played
        //Top25 Most Played
        

        Cursor cursor = null;
        try {
            cursor = getContext().getContentResolver().query(MediaConsts.PLAYLIST_CONTENT_URI, null, null, null,
                    MediaConsts.AudioPlaylist.DATE_ADDED);
            int index = 0;
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    PlaylistInfo info = new PlaylistInfo();
                    info.id = cursor.getLong(cursor
                            .getColumnIndex(MediaConsts.AudioPlaylist._ID));
                    info.name = cursor.getString(cursor
                            .getColumnIndex(MediaConsts.AudioPlaylist.NAME));
                    info.track = index++;
                    info.modified = cursor.getLong(cursor
                            .getColumnIndex(MediaConsts.AudioPlaylist.DATE_MODIFIED));
                    if(info.modified == 0){
                        info.modified = cursor.getLong(cursor
                                .getColumnIndex(MediaConsts.AudioPlaylist.DATE_ADDED));
                        if(info.modified == 0){
                            info.modified = now;
                        }
                    }

                    list.add(info);
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return list;
    }
    
}