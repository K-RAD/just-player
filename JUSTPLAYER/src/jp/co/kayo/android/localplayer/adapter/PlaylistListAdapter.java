package jp.co.kayo.android.localplayer.adapter;
/***
 * Copyright (c) 2010-2012 yokmama. All rights reserved.
 * 
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program; if not, write to the Free Software Foundation, Inc., 59 Temple
 * Place, Suite 330, Boston, MA 02111-1307 USA
 * 
 */

import java.util.Calendar;
import java.util.List;

import jp.co.kayo.android.localplayer.R;
import jp.co.kayo.android.localplayer.util.Funcs;
import jp.co.kayo.android.localplayer.util.ViewCache;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfo;
import jp.co.kayo.android.localplayer.util.bean.PlaylistInfoLoader;
import android.content.Context;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class PlaylistListAdapter extends ArrayAdapter<PlaylistInfo> {
    LayoutInflater inflator;
    Context context;
    ViewCache viewCache;
    Handler handler = new Handler();
    java.text.DateFormat format;

    public PlaylistListAdapter(Context context, List<PlaylistInfo> list, ViewCache cache) {
        super(context, R.layout.playlist_list_row, list);
        this.context = context;
        this.viewCache = cache;
        this.context = context;
        format = DateFormat.getLongDateFormat(context);
    }
    
    public LayoutInflater getInflator() {
        if (inflator == null) {
            inflator = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        return inflator;
    }

    
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = getInflator().inflate(R.layout.playlist_list_row, parent, false);
            holder = new ViewHolder();
            holder.textTrack = (TextView) convertView.findViewById(R.id.textTrack);
            holder.textName = (TextView) convertView.findViewById(R.id.textName);
            holder.textModified = (TextView) convertView.findViewById(R.id.textModified);
            holder.textLabel = (TextView) convertView.findViewById(R.id.textLabel);
            holder.background = convertView.findViewById(R.id.background);
            holder.relativeLayout1 = convertView.findViewById(R.id.RelativeLayout1);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }
        
        if (position % 2 == 1) {
            holder.background.setVisibility(View.GONE);
        } else {
            holder.background.setVisibility(View.VISIBLE);
        }
        
        PlaylistInfo item = getItem(position);
        if(item.id <= PlaylistInfoLoader.PLAYLIST_ORDER_ID){
            holder.relativeLayout1.setVisibility(View.GONE);
            holder.textLabel.setVisibility(View.VISIBLE);

            holder.textLabel.setText(item.name);
        }
        else{
            holder.relativeLayout1.setVisibility(View.VISIBLE);
            holder.textLabel.setVisibility(View.GONE);
            
            holder.textTrack.setText(Funcs.getTrack(position + 1));
            holder.textName.setText(item.name);
            holder.textModified.setText(format.format(item.modified));
        }

        return convertView;
    }
    
    public class ViewHolder {
        private TextView textTrack;
        private TextView textName;
        private TextView textModified;
        private TextView textLabel;
        private View background;
        private View relativeLayout1;
    }
}
